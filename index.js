'use strict'
const { PORT } = require('./config')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

require('./models/conexion')
app.use(require('./routes'))

app.listen(PORT, () => { console.log(`Escuchando puerto ${ PORT }`) })
