'use strict'

const verificaAdminRol = (req, res, next) => {
    let { rol } = req.usuario

    if (rol === 'ADMIN') 
        next()
    else {
        return res.status(401).json({ message: `No se puede proceder con la tarea siguiente porque el usuario no es administrador 😕` })
    }
}

module.exports = verificaAdminRol