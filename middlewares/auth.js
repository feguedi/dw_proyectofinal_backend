'use strict'
const emojis = require('emoji-regex')
const jwt = require('jsonwebtoken')
const Usuario = require('../models/usuario')
const { SECRET_KEY } = require('../config')

const verificarToken = (req, res, next) => {
    Object.keys(req.body).forEach(el => console.log(`body-${ el }: ${ req.body[el] }`))
    Object.keys(req.headers).forEach(el => console.log(`headers-${ el }: ${ req.headers[el] }`))

    const bearerHeader = req.headers['authorization']

    if(typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearerToken = bearer[1]
        
        jwt.verify(bearerToken, SECRET_KEY, (err, datos) => {
            if (err) return res.status(401).json({ message: `Acceso no autorizado 😅` })
            req.usuario = datos.usuario
            console.log(`ID: ${ datos.usuario._id }`)

            Usuario.findById(datos.usuario._id, (err, usuarioBD) => {
                console.log(`verificarToken: usuarioBD: ${ usuarioBD }`)
                console.log(`verificarToken: datos.usuario.password: ${ datos.usuario.password }`)
                console.log(`verificarToken: usuarioBD.password: ${ usuarioBD.password }`)
                if (err) return res.status(500).json({ message: err.message })
                if (!usuarioBD) return res.status(400).json({ message: `Credenciales incorrectas` })

                if (usuarioBD.password != datos.usuario.password)
                    return res.status(400).json({ message: `Credenciales incorrectas` })

                next()
            })
        })
    } else {
        return res.status(401).json({ message: `Acceso no autorizado 😅` })
    }
}

module.exports = verificarToken