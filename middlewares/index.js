'use strict'
const autorizarToken = require('./auth')
const verificarRol = require('./roles')
const propietario = require('./owner')
const check = require('./check')

module.exports = {
    autorizarToken,
    verificarRol,
    propietario,
    check
}