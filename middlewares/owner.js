'use strict'

const propietario = (req, res, next) => {
    console.log(`Elemento: ${ req.body._id }`)
    console.log(`Elemento: ${ req.usuario.id }`)
    if (req.usuario._id === req.params.id) 
        next()
    else {
        return res.status(401).json({ message: `Ese no eres tú 😉` })
    }
}

module.exports = propietario