'use strict'
const Reporte = require('../models/reporte')

const checarReporte = (req, res, next) => {
    const { rol } = req.usuario
    const usuario_id = req.usuario._id
    const id = req.params.id

    Reporte.findById(id, (err, reporte) => {
        if (err) return res.status(500).json({ message: err })
        if (!reporte) return res.status(404).json({ message: `No existe el reporte` })

        if (reporte.usuario_id == usuario_id) {
            return next()
        } else {
            if (rol === 'ADMIN') {
                return next()
            } else if (rol === 'USER' && reporte.usuario_id !== usuario_id) {
                return res.status(401).json({ message: `No eres dueño del reporte 😉` })
            } else {
                return res.status(401).json({ message: `No se puede proceder con la tarea siguiente porque el usuario no es administrador 😕` })
            }
        }
    })
}

module.exports = checarReporte