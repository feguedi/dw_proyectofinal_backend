'use strict'
const { Schema, model } = require('mongoose')

let impactoValues = {
    values: [0, 1, 2, 3, 4],
    message: '{VALUE} no es un parámetro válido'
}

let ReporteSchema = new Schema({
    usuario_id: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    latitud: {
        type: Number,
        required: true
    },
    longitud: {
        type: Number,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    fecha: {
        type: Date,
        required: true
    },
    impacto: {
        type: Number,
        enum: impactoValues,
        default: 0,
        required: true,
    }
})

module.exports = model('Reporte', ReporteSchema)