'use strict'
const { Schema, model } = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

let rolesValidos = {
    values: ['ADMIN', 'USER'],
    message: '{VALUE} no es un rol válido'
}

let UsuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    nickname: {
        type: String,
        required: [true, 'El nickname es obligatorio'],
        unique: true
    },
    rol: {
        type: String,
        default: 'USER',
        enum: rolesValidos
    },
    telefono: {
        type: String,
        required: [true, 'El teléfono es obligatorio']
    }
})

UsuarioSchema.plugin(uniqueValidator, { message: 'El nickname ya existe' })

module.exports = model('Usuario', UsuarioSchema)