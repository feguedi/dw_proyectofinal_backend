'use strict'
const mongoose = require('mongoose')
const { NODE_ENV, BD_HOST, BD_PORT, BD_NAME, BD_USERNAME, BD_PSSWD } = require('../config')

switch (NODE_ENV) {
    case 'desarrollo':
    case 'remoto':
        let uri = NODE_ENV == 'desarrollo' ? `mongodb://${ BD_HOST }:${ String(BD_PORT) }/${ BD_NAME }` : `mongodb+srv://${ BD_USERNAME }:${ BD_PSSWD }@${ BD_HOST }/${ BD_NAME }`
        console.log(`Datos de la URI: ${ uri }`)
        mongoose.connect(uri, { useCreateIndex: true, useNewUrlParser: true }, (err, res) => {
            if (err) return console.log(`Error en la conexión: ${ err }`)
            console.log(`Base de datos corriendo`)
        })
        break

    default:
        console.log(`models: conexion: Por favor especifica un entorno para la base de datos`)
        break
}