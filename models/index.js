'use strict'
const { SECRET_KEY } = require('../config')
const { sign } = require('jsonwebtoken')
const { hashSync, compareSync } = require('bcrypt')
const { pick } = require('underscore')
const Usuario = require('./usuario')
const Reporte = require('./reporte')

const crearUsuario = (obj = undefined, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    console.log(`crearUsuario: me mandan esto ${ JSON.stringify(obj) }`)

    let { nombre, nickname, password, telefono } = obj
    password = hashSync(password, 10)
    telefono = hashSync(telefono, 10)

    let usuario = new Usuario({
        nombre, nickname, password, telefono
    })

    usuario.save((err, usuarioBD) => {
        if (err) return callback({ message: err.message, status: 400 })
        if (!usuarioBD)
        callback(null, usuarioBD)
    })
}

const verPerfil = (obj, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    console.log(`JSON: datos enviados a verPerfil: ${ JSON.stringify(obj) }`)
    let { _id } = obj
    Usuario.findById(_id, (err, datos) => {
        if (err) return callback({ message: err, status: 400 })
        let resp = {
            id: datos._id,
            nombre: datos.nombre,
            nickname: datos.nickname
        }
        Reporte.find({ usuario_id: resp.id }, '_id descripcion impacto longitud latitud fecha', (err, reportes) => {
            if (err) return callback({ message: err, status: 500 })
            callback(null, { datos_usuario: resp, reportes })
        })
    })
}

const actualizarPerfil = (id, obj, callback) => {

    console.log(`actualizarPerfil:`)
    Object.keys(obj).forEach(el => console.log(`    ${ el }: ${ obj[el] }`))

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`actualizarPerfil: Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { nombre, password, new_password } = obj

    Usuario.findById(id, (err, usuario_login) => {
        if (err) return callback({ message: err, status: 500 })
        if (!usuario_login) return callback({ message: `Credenciales incorrectas`, status: 400 })

        if (!compareSync(password, usuario_login.password))
            return callback({ message: `Credenciales incorrectas`, status: 400 })

        new_password = hashSync(new_password, 10)
        let chng = {
            nombre,
            password: new_password
        }

        Object.keys(chng).forEach(el => { if (!chng[el]) delete chng[el] })

        Usuario.findByIdAndUpdate(id, chng, { new: true, runValidators: true }, (err, usuario_act) => {
            if (err) return callback({ message: err, status: 500 })
            if (!usuario_act) return callback({ message: `Usuario incorrecto`, status: 400 })

            let token = sign({ usuario: usuario_act }, SECRET_KEY, { expiresIn: '7d' })

            let datos = {
                id: usuario_act._id,
                nombre: usuario_act.nombre,
                nickname: usuario_act.nickname,
                token
            }
            callback(null, datos)
        })
    })
}

const entrar = (credenciales, callback) => {

    if (credenciales === undefined || credenciales === null || Object.keys(credenciales).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { nickname, password } = credenciales
    Usuario.findOne({ "nickname": nickname }, (err, usuarioBD) => {
        if (err) return callback({ message: err, status: 500 })
        if (!usuarioBD) return callback({ message: `Credenciales incorrectas`, status: 400 })

        if (!compareSync(password, usuarioBD.password))
            return callback({ message: `Credenciales incorrectas`, status: 400 })

        let token = sign({ usuario: usuarioBD }, SECRET_KEY, { expiresIn: '7d' })
        let { _id, nombre, nickname } = usuarioBD

        callback(null, { id: _id, nombre, nickname, token })
    })
}

const obtenerReportes = callback => {
    Reporte.find({}, '_id usuario_id descripcion impacto longitud latitud fecha', (err, reportesBD) => {
        if (err) return callback({ message: err, status: 500 })
        if (!reportesBD || reportesBD.length == 0) return callback({ message: `No existen reportes`, status: 404 })

        callback(null, reportesBD)
    })
}

const obtenerReporte = (obj, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { _id } = obj
    Reporte.findById(_id, (err, reporteBD) => {
        if (err) {
            if (err.name == 'CastError' && err.kind == 'ObjectId') {
                return callback({ message: `No existe el reporte ${ err.value }`, status: 404 })
            }
            return callback({ message: err, status: 500 })
        }
        if (!reporteBD) return callback({ message: `No existe el reporte ${ _id }`, status: 404 })

        callback(null, reporteBD)
    })
}

const crearReporte = (obj, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { usuario_id, descripcion, fecha, impacto, longitud, latitud } = obj
    let reporte = new Reporte({
        usuario_id, descripcion, fecha, impacto, longitud, latitud
    })

    reporte.save((err, reporteBD) => {
        if (err) return callback({ message: err.message, status: 400 })
        callback(null, { message: `Registro de reporte exitoso`, id: reporteBD._id })
    })
}

const actualizarReporte = (obj, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { id } = obj
    const body = pick(obj, ['descripcion', 'impacto'])

    Reporte.findByIdAndUpdate(id, body, (err, resp) => {
        if (err) return callback({ message: err, status: 500 })
        if (!resp) return callback({ message: resp, status: 404 })
        callback(null, { message: 'Actualización exitosa' })
    })
}

const eliminarReporte = (obj, callback) => {

    if (obj === undefined || obj === null || Object.keys(obj).length === 0) {
        console.log(`Error en envío de datos`)
        return callback({ message: `Error en envío de datos`, status: 400 })
    }

    let { id } = obj
    
    Reporte.findByIdAndRemove(id, (err, resp) => {
        if (err) {
            if (err.name == 'CastError' && err.kind == 'ObjectId') {
                return callback({ message: `No existe el reporte ${ err.value }`, status: 404 })
            }
            return callback({ message: err, status: 500 })
        }
        callback(null, { message: `Reporte ${ resp._id } eliminado` })
    })
}

module.exports = {
    entrar,
    crearUsuario,
    verPerfil,
    actualizarPerfil,
    obtenerReportes,
    obtenerReporte,
    crearReporte,
    actualizarReporte,
    eliminarReporte
}