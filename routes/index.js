'use strict'
const Express = require('express')
const app = Express()

app.use(require('./reporte'))
app.use(require('./usuario'))

module.exports = app