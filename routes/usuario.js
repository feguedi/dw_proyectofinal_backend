'use strict'
const Express = require('express')
let app = Express()
const { pick } = require('underscore')
const { entrar, crearUsuario, verPerfil, actualizarPerfil } = require('../models')
const { autorizarToken, propietario } = require('../middlewares')

app.get('/user', autorizarToken, (req, res) => {
    verPerfil(req.usuario, (err, datos) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json(datos)
    })
})

app.post('/login', (req, res) => {
    let body = req.body
    let { nickname, password } = body

    entrar({ nickname, password }, (err, usuarioBD) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json(usuarioBD)
    })
})

app.post('/user', (req, res) => {
    const body = req.body
    
    crearUsuario(body, (err, usuarioBD) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json({ message: 'Usuario creado exitosamente' })
        // res.json({
        //     nombre: usuarioBD.nombre,
        //     nickname: usuarioBD.nickname
        // })
    })
})

app.put('/user', autorizarToken, (req, res) => {
    console.log(`app.put.user: ${ JSON.stringify(req.body) }`)
    actualizarPerfil(req.usuario._id, req.body, (err, usuarioBD) => { 
        if (err) return res.status(err.status).json({ message: err.message })
        console.log(`app.put.user: ${ JSON.stringify(usuarioBD) }`)
        res.json(usuarioBD)
    })
})

module.exports = app