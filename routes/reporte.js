'use strict'
const Express = require('express')
let app = Express()
const { obtenerReportes, obtenerReporte, crearReporte, actualizarReporte, eliminarReporte } = require('../models')
const { autorizarToken, verificarRol, check } = require('../middlewares')

app.get('/report', (req, res) => {
    obtenerReportes((err, reportes) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json(reportes)
    })
})

app.get('/report/:id', (req, res) => {
    const _id = req.params.id
    obtenerReporte({ _id }, (err, reporte) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json({ 
            usuario_id: reporte.usuario_id,
            descripcion: reporte.descripcion,
            impacto: reporte.impacto,
            latitud: reporte.latitud,
            longitud: reporte.longitud,
            fecha: reporte.fecha
        })
    })
})

app.post('/report', autorizarToken, (req, res) => {
    const { descripcion, latitud, longitud, impacto } = req.body
    const { _id: usuario_id } = req.usuario
    const fecha = Date.now()

    crearReporte({ usuario_id, descripcion, latitud, longitud, fecha, impacto }, (err, reporteBD) => {
        if (err) res.status(err.status).json({ message: err.message })
        res.json({ message: reporteBD.message })
    })
})

app.put('/report/:id', [autorizarToken, verificarRol], (req, res) => {
    const id = req.params.id
    const { descripcion, impacto } = req.body

    actualizarReporte({ id, descripcion, impacto }, (err, resp) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json({ message: resp.message })
    })
})

app.delete('/report/:id', [autorizarToken, check], (req, res) => {
    const id = req.params.id

    eliminarReporte({ id }, (err, resp) => {
        if (err) return res.status(err.status).json({ message: err.message })
        res.json({ message: resp.message })
    })
})


module.exports = app