'use strict'
const dotenv = require('dotenv')
dotenv.config({silent: true})

module.exports = {
    PORT: process.env.PORT,
    NODE_ENV: process.env.NODE_ENV,
    SECRET_KEY: process.env.SECRET_KEY,
    BD_NAME: process.env.BD_NAME,
    BD_USERNAME: process.env.BD_USERNAME,
    BD_PSSWD: process.env.BD_PSSWD,
    BD_HOST: process.env.BD_HOST,
    HOME: process.env.HOME
}