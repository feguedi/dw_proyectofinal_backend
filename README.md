# Hey Listen

## Objetivo

Este proyecto funge como el controlador entre la base de datos y la interfaz gráfica de la aplicación [HeyListen](https://github.com/Mango3313/HeyListenFlutter). Ambos 
proyectos funcionan de manera independiente aunque existe la comunicación entre ellos en un ambiente en producción.

La documentación completa se encuentra en [Gitbook](https://mmano.gitbook.io/heylisten), ahí se explica cómo usar este web service sin la necesidad de usar la aplicación
móvil. Podemos utilizar [Postman](https://www.getpostman.com) como referencia para hacer las solicitudes y visualizar los resultados.